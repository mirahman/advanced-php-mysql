<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo "<pre>";

class Node {
    public $prev;
    public $next;
    public $name;  
    
    public function __construct($name) {
        $this->name = $name;
        $this->prev = null;
        $this->next = null;
    }
    
}

$a = new Node("A");
$b = new Node("B");
$c = new Node("C");
$d = new Node("D");
$e = new Node("E");

$a->next = $b;
//$b->prev = $a;
$b->next = $c;
$c->next = $d;
$e->next = $c->next;
$c->next = $e;

$ar = [$a, $b, $c, $d,$e];

foreach($ar as $obj) {
    echo $obj->name;
    
    if($obj->next) {
        echo " -> ".$obj->next->name;
    }
    
    echo "\n";
}
echo "\n";
function showMeNext($obj) {
    echo $obj->name." -> ";
    if($obj->next) {
        echo showMeNext($obj->next);
    } 
}

showMeNext($a);


$a = new SplFileInfo("a.txt");
//$a->openFile("w");
$a->getSize();
