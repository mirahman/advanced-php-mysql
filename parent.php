<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cricketer {
    public $name = "";
    public $run = "";
    public $level = "";
    private $phoneNumber = "";
    protected $email = "";
    
    
    function __construct ($name= '', $run = 0, $level = 0, $phoneNumber = "", $email = "") {
        $this->run = $run;
        $this->name = $name;
        $this->level = $level;
        $this->phoneNumber = $phoneNumber;
        $this->email = $email;

    } 
    


    function getPhoneNumber() {
        return $this->phoneNumber;
    }
    
    function getName() {
        return $this->name;
    }
    
    function setName($val = "") {
        $this->name = $val;
    }
    
    function getRun() {
        return $this->run;
    }
    
    function setRun($val = "") {
        $this->run = $val;
    }
    
    function testSalary() {
        echo nationalCricketer::getAnnualSalary();
    }
    
    function getLevel() {
        echo "Level from top parent";
    }
    
    private function setLevel($val = "") {
        $this->level = $val;
    }
}


class nationalCricketer extends cricketer {
  
    public $annualSalary = "";
    
    function __construct ($name= '', $run = 0, $level = 0, $phoneNumber = "", $email = "", $salary = "") {
        parent::__construct($name, $run, $level, $phoneNumber, $email);
        $this->annualSalary = $salary;

    } 
    
    public function getAnnualSalary() {
        return $this->annualSalary;
    }
    
    public function setLevel($level = "") {
        echo "testing level";
    }
    
    public function getLevel() {
        echo "testing level from 1st level child";
    }
    
    public function getParentLevel() {
        parent::getLevel();
    }
}

class internationalCricket extends nationalCricketer {
    
    function test() {
        cricketer::getName();
        parent::getAnnualSalary();
    }
    
     public function getLevel() {
        echo "testing level from 2nd level child";
    }
    
    public function getParentLevel() {
        parent::getLevel();
    }
    
    public function getParentsParentLevel() {
        parent::getParentLevel();
        // cricketer::getLevel();
    }
}

$mash = new internationalCricket();


echo $mash->getLevel()."<br />";
echo $mash->getParentLevel()."<br />";
echo $mash->getParentsParentLevel()."<br />";




