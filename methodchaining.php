<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cricketer {
    
    public $name;
    public $run;
    
    public function __construct($name = "", $run = "") {
       $this->name = $name;
       $this->run = $run;               
    }
    
    public function setName($name) {
        $this->name = $name;
        
        return $this;
    }
    
    public function getName() {
        echo $this->name;
        return $this;
    }
    
    public function setRun($run) {
        $this->run = $run;
        return $this;
    }
    
    public function getRun() {
        echo $this->run;
        return $this;
    }
}

$sakib = new cricketer;
$sakib->setName("Sakib");
$sakib->setRun(12345);
$sakib->getName();
$sakib->getRun();

$sakib->setName("Sakib")->setRun(12345)->getName()->getRun();