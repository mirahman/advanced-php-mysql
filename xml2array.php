<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$xml = <<<XML
<students>

   <student>
        <name>Test 1</name>
        <age>20</age>
        <grade>2</grade>
   </student>
        
        
   <student>
        <name>Test 2</name>
        <age>20</age>
        <grade>2</grade>
   </student>
        
        
        
   <student>
        <name>
            <firstname>Test </firstname>
        <lastname>string</lastname>
        <nickname>Test</nickname>
                </name>
        <age>20</age>
        <grade>2</grade>
   </student>
</students>
        
XML;
        

//$xmlObj = simplexml_load_string($xml);

/*
$xmlObj = simplexml_load_file("./test.xml");

foreach($xmlObj->xpath("//student") as $student) {
    echo $student->name."<br />";
}
 * 
 */

print_r(xml_to_array($xml));

function xml_to_array($xml,$main_heading = '') {
    $deXml = simplexml_load_string($xml);
    $deJson = json_encode($deXml);
    $xml_array = json_decode($deJson,TRUE);
    if (! empty($main_heading)) {
        $returned = $xml_array[$main_heading];
        return $returned;
    } else {
        return $xml_array;
    }
}