<?php

class Obj {

    public $id;
    public $size;
    public $rong;  // store color object
    public $dim;

    function __construct($id, $size, Color $color, Dimension $dim) {
        $this->id = $id;
        $this->size = $size;
        $this->rong = $color;
        $this->dim = $dim;
    }
    
    
    function __clone() {
        foreach ($this as $key => $val) {
            if (is_object($val) || (is_array($val))) {
                $this->{$key} = unserialize(serialize($val));
            }
        }
    }
      

}

class Color {

    public $green;
    public $blue;
    public $red;

    function __construct($green, $blue, $red) {
        $this->green = $green;
        $this->blue = $blue;
        $this->red = $red;
    }

}

class Dimension {
     public $width;
     public $height;
     
     
     public function __construct($width, $height) {
         $this->width = $width;
         $this->height = $height;
     }
}

$color = new Color(23, 42, 223);


$dim = new Dimension (50, 10);

$obj1 = new Obj(23, "small", $color, $dim);

$obj2 = new Obj(23, "small", "test", $dim); // will cause fatal error

